static const char *CopyrightIdentifier(void) { return "@(#)sompdmpf.cc Copyright (c) 1993-2022, David A. Clunie DBA PixelMed Publishing. All rights reserved."; }
#include "sompdmp.h"
#include "sompptrs.h"
#include "sompdmpf.h"
